#__________________________________________________________
#
#   Imports
#__________________________________________________________

import pandas as pd
import matplotlib.pyplot as plt
import os

#__________________________________________________________
#
#   Importing the CSV
#__________________________________________________________

Coffeebar = pd.DataFrame.from_csv('Coffeebar_2013-2017.csv',
                                  header=0,
                                  sep=';',
                                  index_col=None,
                                  parse_dates=True,
                                  encoding=None,
                                  tupleize_cols=None,
                                  infer_datetime_format=False)

#__________________________________________________________
#
#   Displaying the unique values
#__________________________________________________________

# DRINKS
#----------------------------------------------------------
CoffeebarDrinks = Coffeebar['DRINKS'].unique()
print("__________________________________________________")
print()
print("The drinks sold are :")
print(', '.join(CoffeebarDrinks))
print("__________________________________________________")
print()

# FOOD
#----------------------------------------------------------
CoffeebarFood = Coffeebar['FOOD'].unique()
CoffeebarFood = CoffeebarFood[~pd.isnull(CoffeebarFood)]
print("The food sold is :")
print(', '.join(CoffeebarFood))
print("__________________________________________________")
print()


#__________________________________________________________
#
# Counting the unique customer (= unique values in CustomerID)
#__________________________________________________________

#We use the function nunique() in the column CUSTOMER
#----------------------------------------------------------
CoffeebarUnique = Coffeebar['CUSTOMER'].nunique()
print("There are ",CoffeebarUnique," unique customers.")
print("__________________________________________________")
print()

#__________________________________________________________
#
#   Counting the total quantity of drinks & food sold
#__________________________________________________________

# DRINKS
#----------------------------------------------------------
DrinkSold = []
TotalDrinks = 0

print("QUANTITY OF DRINKS SOLD")
print()
for i in range(len(CoffeebarDrinks)):
    Drinks = Coffeebar['DRINKS'].str.count(CoffeebarDrinks[i]).sum()
    DrinkSold.append(Drinks)
    TotalDrinks = TotalDrinks + Drinks
    print(CoffeebarDrinks[i]," : ",Drinks)
print()
print("TOTAL : ",TotalDrinks)
print("__________________________________________________")
print()



# FOOD
#----------------------------------------------------------
FoodSold = []
TotalFood = 0

print("QUANTITY OF FOOD SOLD")
print()
for i in range(len(CoffeebarFood)):
    Food = int(Coffeebar['FOOD'].str.count(CoffeebarFood[i]).sum())
    FoodSold.append(int(Food))
    TotalFood = TotalFood + Food
    print(CoffeebarFood[i]," : ",Food)
print()
print("TOTAL : ",TotalFood)
print("__________________________________________________")
print()


#__________________________________________________________
#
#   Displaying the charts of sold food and drinks
#__________________________________________________________

#   Plots with points
#__________________________________________________________

# DRINKS
#----------------------------------------------------------
# fig = plt.figure(1)
# plt.plot(CoffeebarDrinks, DrinkSold, 'ro')
# plt.xlabel('Drinks')
# plt.ylabel('Amount sold')
# plt.title('Drinks sold over 5 years')
# plt.grid(False)
# plt.show()

# FOODS
#----------------------------------------------------------
# fig = plt.figure(1)
# plt.plot(CoffeebarFood, FoodSold, 'ro')
# plt.xlabel('Food')
# plt.ylabel('Amount sold')
# plt.title('Food sold over 5 years')
# plt.grid(False)
# plt.show()


# Bar charts (better looking)
#__________________________________________________________

# DRINKS
#----------------------------------------------------------
fig = plt.figure(1)
x = CoffeebarDrinks
height = DrinkSold
width = 0.5
plt.bar(x, height, width, color='b' )
plt.xlabel('Drinks')
plt.ylabel('Amount sold')
plt.title('Drink sold over 5 years')
plt.show()
# We save the charts as png files
fig.savefig(os.path.abspath("../ExamProject_Ewa_AnneLaure/DrinkSold.png"))
#
# FOOD
#----------------------------------------------------------
fig = plt.figure(1)
x = CoffeebarFood
height = FoodSold
width = 0.5
plt.bar(x, height, width, color='b' )
plt.xlabel('Food')
plt.ylabel('Amount sold')
plt.title('Food sold over 5 years')
plt.show()
# We save the charts as png files
fig.savefig(os.path.abspath("../ExamProject_Ewa_AnneLaure/FoodSold.png"))


#__________________________________________________________
#
#   Computing probabilities using minutes as intervals
#__________________________________________________________

# Convert data time into date
Coffeebar['TIME']=pd.to_datetime(Coffeebar['TIME'])

# DRINKS
#----------------------------------------------------------
# Interval hours and minutes : counting total drinks
HoursMinD=Coffeebar.groupby(Coffeebar['TIME'].dt.strftime("%H:%M"))['DRINKS'].count()
# Counting each type of drinks for each timestamps
HoursMinDrinks=Coffeebar.groupby(Coffeebar['TIME'].dt.strftime("%H:%M"))['DRINKS'].value_counts()
# Passing the results to Data Frame
HoursMinDrinks= pd.DataFrame(HoursMinDrinks)
# Change the column name
HoursMinDrinks.rename(index=str, columns={'DRINKS':'PROBABILITY'},inplace=True)
# Pass index to columns
HoursMinDrinks.reset_index(inplace=True)
# Pass time to index (otherwise we have an index difficult to use, the timestamp as an index is more efficient)
HoursMinDrinks.set_index('TIME', inplace=True)
# Computing probabilities : we divide the specific amounts by the total amounts in each timestamp
HoursMinDrinks['PROBABILITY'] = HoursMinDrinks['PROBABILITY']/HoursMinD
# Overview of the result (10 first)
print("DRINKS : Probabilities of consumption")
print()
print(HoursMinDrinks.head(10))
# Export the data frame to csv
datapath = os.path.abspath("../ExamProject_Ewa_AnneLaure/ComputedDrinksProba.csv")
HoursMinDrinks.to_csv(datapath, sep=';')


# FOOD
#----------------------------------------------------------
# Replace missing values by Nothing
Coffeebar['FOOD'].fillna("nothing", inplace=True)
# Interval hours and minutes : counting total food
HoursMinF=Coffeebar.groupby(Coffeebar['TIME'].dt.strftime("%H:%M"))['FOOD'].count()
# Counting type of food for each timestamps
HoursMinFood=Coffeebar.groupby(Coffeebar['TIME'].dt.strftime("%H:%M"))['FOOD'].value_counts(dropna= False)
# Passing the results to Data Frame
HoursMinFood= pd.DataFrame(HoursMinFood)
# Change the column name
HoursMinFood.rename(index=str, columns={'FOOD': 'PROBABILITY'}, inplace=True)
# Pass index to columns
HoursMinFood.reset_index(inplace=True)
# Pass time to index
HoursMinFood.set_index('TIME', inplace=True)
# Computing probabilities
HoursMinFood['PROBABILITY'] = HoursMinFood['PROBABILITY']/HoursMinF
# Overview of the result (10 last)
print()
print("__________________________________________________")
print()
print("FOOD : Probabilities of consumption")
print()
print(HoursMinFood.tail(10))

# Export to csv
datapath = os.path.abspath("../ExamProject_Ewa_AnneLaure/ComputedFoodProba.csv")
HoursMinFood.to_csv(datapath, sep=';')
