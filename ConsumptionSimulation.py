#__________________________________________________________
#
#   Imports
#__________________________________________________________

#We import the Customer class and the function convert_proba
from CustomerClasses import Customer
from CustomerClasses import convert_proba
from CustomerClasses import prices
import random
import pandas as pd
import os
import matplotlib.pyplot as plt

#__________________________________________________________
#
#   Data
#__________________________________________________________

#We import the CSV files in order to manipulate the data
datapath = os.path.abspath('../ExamProject_Ewa_AnneLaure/ComputedFoodProba.csv')
df_probas_f= pd.read_csv(datapath, sep=';')
datapath = os.path.abspath('../ExamProject_Ewa_AnneLaure/ComputedDrinksProba.csv')
df_probas_d= pd.read_csv(datapath, sep=';')

# Create list of returning customers
list_returning = []
for i in range(0, 1000):
    client = Customer("Name")
    if random.uniform(0, 1) <= 2/3:
        client.Profile = "Regular"
        client.Budget = 250
    else:
        client.Profile = "Hipster"
        client.Budget = 500
    client.CustomerID = "%s_%s" % (client.Profile, i)
    list_returning.append(client)

# Convert dataframe
ar_probas_f = convert_proba(df_probas_f)
ar_probas_d = convert_proba(df_probas_d)

#Initilization of lists
Simulation_Food = []
Simulation_Drink = []
Simulation_Price = []
Simulation_ID = []
Simulation_Date = []

# For each day
for date in range(0,1825):
    # For each timestamp
    for (index,i) in enumerate(ar_probas_d):
        sim_date = "%s %s" % (date, i[0][0])
        sim_d = ar_probas_d[index]
        sim_f = ar_probas_f[index]
        # if Onetime
        if random.uniform(0, 1) <= 0.8:
            # if trip advisor
            if random.uniform(0, 1) <= 0.1:
                client = Customer("TripAdvisor")
                client.Profile ="TripAdvisor"
                client.CustomerID = "%s_%s" % (client.Profile, index)
            else:
                client = Customer("OneTime")
                client.Profile = "OneTime"
                client.CustomerID = "%s_%s" % (client.Profile, index)
            client.Simulation_Based_Proba(sim_f,sim_d , prices,sim_date)
            Simulation_Food.append(client.History[0][0])
            Simulation_Drink.append(client.History[1][0])
            Simulation_Price.append(client.History[2][0])
            Simulation_ID.append(client.CustomerID)
            Simulation_Date.append(sim_date)
        else:
            # list_returning
            rnd= random.randint(0, len(list_returning)-1)
            list_returning[rnd].Simulation_Based_Proba(sim_f,sim_d , prices,sim_date)
            Simulation_Food.append(list_returning[rnd].History[0][len(list_returning[rnd].History[0])-1])
            Simulation_Drink.append(list_returning[rnd].History[1][len(list_returning[rnd].History[1])-1])
            Simulation_Price.append(list_returning[rnd].History[2][len(list_returning[rnd].History[2])-1])
            Simulation_ID.append(list_returning[rnd].CustomerID)
            Simulation_Date.append(sim_date)
            #If there is no budget left, we erase the delete the customer
            if list_returning[rnd].Budget < 5:
                del list_returning[rnd]

#We store the results into a data frame
Simulation_Table= pd.DataFrame({'TIME': Simulation_Date, 'DRINK': Simulation_Drink,'FOOD': Simulation_Food,'CUSTOMERID': Simulation_ID,})
#We split the column time : it contains days and hours in the same column and we want two separate columns
Simulation_Date = pd.DataFrame(Simulation_Table.TIME.str.split(' ',3).tolist(),
                                   columns = ['DAYS','TIME'])
#We regroupe all the columns into a single data frame
Simulation_Table= pd.DataFrame({'DAYS': Simulation_Date['DAYS'], 'TIME': Simulation_Date['TIME'], 'DRINK': Simulation_Drink,'FOOD': Simulation_Food,'CUSTOMERID': Simulation_ID,})

#We print the result : first 10 and last 10 entries
print()
print("__________________________________________________")
print()
print("First 10 entries : ")
print()
print(Simulation_Table.head(10))
print()
print("__________________________________________________")
print()
print("Last 10 entries : ")
print()
print(Simulation_Table.tail(10))

#We save the results into a CSV file
datapath = os.path.abspath("../ExamProject_Ewa_AnneLaure/ConsumptionSimulation.csv")
Simulation_Table.to_csv(datapath, sep=';')


CashFlow = []

#fWe want to return the price of drinks and food
def  drink_price(drink) :
    if drink == 'milkshake':
        return 5
    elif drink == 'frappucino':
        return 4
    elif drink == 'water':
         return 2
    elif (drink == 'coffee') or  (drink =='soda') or (drink =='tea') :
         return 3
    else :
        return 0

def food_price(food):
    if food == 'sandwich':
        return 5
    elif food == 'cookie':
        return 2
    elif (food == 'muffin') or (food == 'pie'):
        return 3
    else :
        return 0

#Computing drinks sold per day
DailyDrinks = Simulation_Table.groupby(Simulation_Table['DAYS'])['DRINK'].value_counts()
DailyDrinks = pd.DataFrame(DailyDrinks)
DailyDrinks.rename(index=str, columns={'DRINK':'QUANTITY'},inplace=True)
DailyDrinks.reset_index(inplace=True)
DailyDrinks.set_index('DAYS', inplace=True)
DailyDrinks['PRICES'] = DailyDrinks['DRINK']

for i in range(0,1825):
    DailyDrinks.at[str(i),'PRICES'] = drink_price(DailyDrinks.iloc[i][0])

DailyDrinks['CASHFLOW'] = DailyDrinks['PRICES'] * DailyDrinks['QUANTITY']

# #Trying to do the sum, but does not work
# for i in range(0,1825):
#     CashFlow.append(DailyDrinks.iloc[i][4].sum())



#Computing food sold per day
DailyFood = Simulation_Table.groupby(Simulation_Table['DAYS'])['FOOD'].value_counts()
DailyFood = pd.DataFrame(DailyFood)
DailyFood.rename(index=str, columns={'FOOD':'QUANTITY'},inplace=True)
DailyFood.reset_index(inplace=True)
DailyFood.set_index('DAYS', inplace=True)

for i in range(0,1825):
    DailyFood.at[str(i),'PRICES'] = food_price(DailyFood.iloc[i][0])

DailyFood['CASHFLOW'] = DailyFood['PRICES'] * DailyFood['QUANTITY']


# Was supposed to be the plot
# fig = plt.figure(1)
# plt.plot(CoffeebarDrinks, ListDailyDrinks, 'ro')
# plt.xlabel('Drinks')
# plt.ylabel('Amount sold')
# plt.title('Drinks sold over 5 years')
# plt.grid(False)
# plt.show()
