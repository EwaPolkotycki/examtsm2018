#__________________________________________________________
#
#   Imports
#__________________________________________________________

import random
import pandas as pd
import os
import sys

#__________________________________________________________
#
#   Data
#__________________________________________________________

datapath = os.path.abspath('../ExamProject_Ewa_AnneLaure/ComputedFoodProba.csv')
df_probas_f= pd.read_csv(datapath, sep=';')
datapath = os.path.abspath('../ExamProject_Ewa_AnneLaure/ComputedDrinksProba.csv')
df_probas_d= pd.read_csv(datapath, sep=';')


#__________________________________________________________
#
#   Class creation : Customer
#__________________________________________________________

#Here we simply create the class customer
#We do not define the Profile and the Budget yet
#We just make a precision on the tip for the TripAdvisor Profile

class Customer(object):
    def __init__(self, CustomerID):
        # Initialize instance attributes
        self.CustomerID = CustomerID
        self.History = [[],[],[],[]] # food, drink, price, date
        # Initialize the Profile and the Budget
        self.Profile = ""
        self.Budget = 0

    def Simulation_Based_Proba(self, probas_f, probas_d, prices, date):
        price=0
        # Random draw : we pick a random number between 0 and 1 which will represent a probability in our simulations
        rand = random.uniform(0, 1)
        counter = 0
        for value_proba in probas_f:
            counter += value_proba[2]
            # If we reach a value superior to the random number we select the corresponding element name
            if rand <= counter:
                food=value_proba[1]
                break # Once an element is chosen we must quit the loop
        # Random draw
        rand = random.uniform(0, 1)  #random float between 0 and 1
        counter = 0
        for value_proba in probas_d:
            counter += value_proba[2]
            # If we reach a value superior to the random number we select the corresponding element name
            if rand <= counter:
                drink=value_proba[1]
                break # Once an element is chosen we must quit the loop
        # Add price
        for i in prices:
            if i[0] == food:
                price += i[1]
            if i[0] == drink:
                price += i[1]
        if self.Profile == "TripAdvisor":
            price += random.randint(0,10)
        self.Budget = self.Budget - price
        # Add in history
        self.History[0].append(food)
        self.History[1].append(drink)
        self.History[2].append(price)
        self.History[3].append(date)

#__________________________________________________________
#
#   Definition of a function to convert the dataframe
#       into a list of list of tuples (we need it
#           to do the simulation)
#__________________________________________________________

def convert_proba(df_probas):
    # Create a series of values counted by timestamp
    df_probas.reset_index(inplace=True)
    df_probas_index = df_probas.groupby(df_probas['TIME'])['TIME'].value_counts()
    # Instantiate arrays & counter
    ar_probas = []
    temp=[]
    counter=0
    # For each timestamp
    for i in range(0, len(df_probas_index)):
        # Read all values
        for j in range(df_probas_index.iloc[i]):
            #Add as tuples into array
            temp.append(tuple(df_probas.iloc[counter][1:]))
            counter += 1
        #Add to array of array
        ar_probas.append(temp)
        temp = []
    # Return array of arrays
    return ar_probas

#__________________________________________________________
#
#   Simulation for two customers : Ewa and Anne-Laure
#__________________________________________________________

prices = [['sandwich', 5], ['cookie', 2], ['pie', 3], ['muffin', 3], ['milkshake', 5], ['frappucino', 4], ['water', 2], ['coffee',3], ['tea', 3], ['soda', 3]]

#At what time do we want to order ?
#Enter an INPUT value
print("__________________________________________________")
print()
Time = input("For which timestamp do you want to compute probabilities ?")

# Convert dataframe
ar_probas_f = convert_proba(df_probas_f)
ar_probas_d = convert_proba(df_probas_d)

#Getting the index according to the time previously chosen
for i in ar_probas_f:
    if i[0][0] == Time:
        TimeIndex = [y[0] for y in ar_probas_f].index(i[0])

for i in ar_probas_f:
    if i[0][0] == Time:
        TimeIndex = [y[0] for y in ar_probas_f].index(i[0])

try:
    ar_probas_f = ar_probas_f[TimeIndex]
except:
    print()
    print("------------------ CAUTION -------------------")
    print()
    print("!!! Make sure the timestamp exists and its format is alright : HH:MM !!!")
    print()
    print("Run the code again.")
    print()
    print("----------------------------------------------")
    sys.exit()

try:
    ar_probas_d = ar_probas_d[TimeIndex]
except:
    print()
    print("------------------ CAUTION -------------------")
    print()
    print("!!! Make sure the timestamp exists and its format is alright : HH:MM !!!")
    print()
    print("Run the code again.")
    print()
    print("----------------------------------------------")
    sys.exit()


# Testing of the functioning
# Defining name and Profile
Ewa = Customer("Ewa Polkotycki")
Ewa.Profile = 'Hipster'
AnneLaure = Customer("Anne-Laure Dubresson")
AnneLaure.Profile = 'TripAdvisor'

#We suppose Ewa comes 10 times on a given date x, and Anne-Laure one time
for i in range(10):
    Ewa.Simulation_Based_Proba(ar_probas_f,ar_probas_d,prices, Time)
print("__________________________________________________")
print()
print("EWA'S HISTORY :")
print("Profile : ", Ewa.Profile)
print()
print(pd.DataFrame(Ewa.History).rename(index={0:'Food', 1:'Drink', 2:'Cost', 3:'Time'}).transpose())

AnneLaure.Simulation_Based_Proba(ar_probas_f,ar_probas_d,prices,Time)
print()
print("__________________________________________________")
print()
print("ANNE-LAURE'S HISTORY :")
print("Profile : ", AnneLaure.Profile)
print()
print(pd.DataFrame(AnneLaure.History).rename(index={0:'Food', 1:'Drink', 2:'Cost', 3:'Time'}).transpose())
